from channels.db import database_sync_to_async
from django.db.models import Count

from murr_websocket.consumers.base import BaseMurrWebSocketConsumer
from murr_websocket.models import MurrWebSocket, MurrWebSocketType


class MurrBattleRoomConsumer(BaseMurrWebSocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope['murr_ws_type'] = 'murr_battle_room'

    async def connect(self):
        await super().connect()
        await self.channel_layer.group_add(self.scope['murr_ws_name'], self.channel_name)
        murr_battle_rooms_available = await self.get_murr_battle_rooms_available()
        for i, room in enumerate(murr_battle_rooms_available):
            murren_in_battle_room_data = await self.get_websocket_data(room['id'])
            murr_battle_rooms_available[i]['murrens_in_ws_data'] = murren_in_battle_room_data['murrens_in_ws_data']
            murr_battle_rooms_available[i]['murrens_ws_link'] = murren_in_battle_room_data['murr_ws_link']
            murr_battle_rooms_available[i]['murr_ws_type'] = murr_battle_rooms_available[i]['murr_ws_type'].value
            murr_battle_rooms_available[i]['murr_websocket_id'] = murr_battle_rooms_available[i]['id']

        await self._group_send(murr_battle_rooms_available, gan='murrenConnectToBattleLobby')

    @database_sync_to_async
    def get_murr_battle_rooms_available(self):
        return list(MurrWebSocket.objects.annotate(murr_ws_members_count=Count('murr_ws_member', distinct=True)).filter(
            murr_ws_members_count=1, murr_ws_type=MurrWebSocketType.MURR_BATTLE_ROOM).values())

    async def disconnect(self, code):
        destroyed_murr_battle_room_ids = await self.destroy_created_murr_battle_room(self.scope['murren'])
        await self._group_send(destroyed_murr_battle_room_ids,
                               gan='disconnect_MurrBattleRoomConsumer')
        await self.channel_layer.group_discard(self.scope['murr_ws_name'], self.channel_name)
        await super().disconnect(code=code)

    @database_sync_to_async
    def destroy_created_murr_battle_room(self, murren):
        destroyed_murr_battle_room_ids = []

        for i in MurrWebSocket.objects.filter(murr_ws_member__murr_ws_member=murren,
                                              murr_ws_type=MurrWebSocketType.MURR_BATTLE_ROOM):
            destroyed_murr_battle_room_ids.append(i.id)
            i.delete()
        return destroyed_murr_battle_room_ids

    async def gan__murren_leave_battle_room(self, gan):
        murren_leaver = gan['data'].get('murren_name')
        data = {
            'murren_leaver_name': murren_leaver
        }
        return await self._group_send(data, gan=gan['gan'])
