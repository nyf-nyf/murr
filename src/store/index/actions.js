export const actions = {
  async changeShowSignUpForm_actions(context) {
    context.commit("changeShowRegisterForm_mutations");
  },
  async changeShowLoginForm_actions(context) {
    context.commit("changeShowLoginForm_mutations");
  },
  async changeShowResetPasswordForm_actions(context) {
    context.commit("changeShowResetPasswordForm_mutations");
  },
};
