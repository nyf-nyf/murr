import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import auth from "./auth/auth";
import showPopUpMessage from "./showPopUpMessage";
import restrictedAccess from "./restricted-access";
import { actions } from "./index/actions";
import { mutations } from "./index/mutations";
import { state } from "./index/state";
import { getters } from "./index/getters";
import comments from "../components/murr_comment/store";
import modal from "../components/modal/store";
import murrCard from "../components/murr_card/store";
import murren from "../components/murren/store";
import murr_game from "../components/murr_game/store/murr_game";
import murr_chat from "../components/murr_chat/store";
import notification from "../components/notification/store";

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  plugins: [
    createPersistedState({
      paths: ["auth.currentMurren", "murrCard.murrDataLocalStorage"],
    }),
  ],
  modules: {
    auth,
    showPopUpMessage,
    restrictedAccess,
    comments,
    modal,
    murrCard,
    murren,
    murr_game,
    murr_chat,
    notification,
  },
});
