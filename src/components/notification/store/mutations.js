import * as type from "./type.js";

export default {
  [type.ADD_MURR_CARD_NOTIFY]: (state, murrId) => {
    state.murrCardNotices = [...state.murrCardNotices, murrId];
  },
  [type.REMOVE_MURR_CARD_NOTIFY]: (state, murrId) => {
    state.murrCardNotices = state.murrCardNotices.filter((id) => id !== murrId);
  },
};
